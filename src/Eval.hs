import System.Environment
import Language.Haskell.Interpreter
import System.Posix.Resource
import System.Timeout

main :: IO ()
main = do
  setResourceLimit ResourceCPUTime $ ResourceLimits (ResourceLimit 2) (ResourceLimit 10)
  [block] <- getArgs
  Just (Right s) <- timeout 2500000 $ runInterpreter $ do
    setImports ["Prelude"]
    eval block
  let p = if length s >= 1900 then take 1893 s ++ "..." else s
  putStr p
