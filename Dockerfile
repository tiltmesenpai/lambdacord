FROM registry.gitlab.com/jkoike/lambdacord:pre_build
COPY . .
RUN stack install && \
    stack install hoogle && \
    rm -r ~/.stack /.stack-work && \
    hoogle generate
ENTRYPOINT lambdacord +RTS -T
